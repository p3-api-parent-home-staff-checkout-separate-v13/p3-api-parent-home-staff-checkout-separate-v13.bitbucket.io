/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8880541505504315, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.17232142857142857, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9989703644014684, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.549579045837231, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.11317567567567567, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.002369668246445498, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.3151183970856102, 500, 1500, "me"], "isController": false}, {"data": [0.35233644859813085, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.0678391959798995, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.4916317991631799, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.0, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 26888, 0, 0.0, 559.5095953585228, 10, 16896, 56.0, 1367.2000000000116, 3841.5500000000065, 8513.0, 88.82839548590005, 184.84086615056853, 81.09828012263128], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 560, 0, 0.0, 2682.578571428572, 235, 8678, 2278.5, 5252.200000000002, 6408.849999999996, 7853.579999999997, 1.8517844919662314, 1.170023990529445, 2.2912216321496244], "isController": false}, {"data": ["getLatestMobileVersion", 22338, 0, 0.0, 66.73148894260903, 10, 1392, 47.0, 126.0, 168.0, 292.9900000000016, 74.44883267509873, 49.80219763910413, 54.89147331025346], "isController": false}, {"data": ["findAllConfigByCategory", 1069, 0, 0.0, 1408.0037418147783, 38, 7731, 646.0, 3767.0, 4794.0, 7074.899999999999, 3.5382471609621122, 4.0012599730411385, 4.595574925858993], "isController": false}, {"data": ["getNotifications", 296, 0, 0.0, 5092.6047297297255, 139, 11509, 5440.5, 8477.0, 8616.8, 9794.239999999994, 0.9781728656168933, 8.132017192379504, 1.0880262636109779], "isController": false}, {"data": ["getHomefeed", 211, 0, 0.0, 7142.042654028435, 1127, 16532, 7612.0, 9571.800000000001, 9930.8, 15756.759999999995, 0.6972164212641095, 8.495282507922163, 3.4888056079661105], "isController": false}, {"data": ["me", 549, 0, 0.0, 2743.0218579234984, 88, 8290, 2154.0, 6316.0, 7402.5, 8155.5, 1.8166115727091336, 2.3939871739033323, 5.756742727969862], "isController": false}, {"data": ["findAllChildrenByParent", 535, 0, 0.0, 2815.6542056074777, 95, 8706, 1935.0, 6824.200000000001, 8006.8, 8442.119999999999, 1.7701867464298475, 1.9880026937444577, 2.8281499191008113], "isController": false}, {"data": ["getAllClassInfo", 199, 0, 0.0, 7578.281407035179, 126, 16896, 8499.0, 10630.0, 13590.0, 16634.0, 0.6575621297082606, 1.8894649728383885, 1.6882137099638836], "isController": false}, {"data": ["findAllSchoolConfig", 956, 0, 0.0, 1572.2489539748942, 46, 7873, 916.0, 3769.5000000000023, 4928.799999999999, 6548.939999999992, 3.1644417375300806, 69.01202429933764, 2.3207966258643458], "isController": false}, {"data": ["getChildCheckInCheckOut", 175, 0, 0.0, 8618.411428571426, 2447, 13356, 8621.0, 10120.2, 11750.8, 13289.880000000001, 0.5781645423249483, 38.54900793675375, 2.660460276792145], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 26888, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
